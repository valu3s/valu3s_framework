from Products.CMFCore.interfaces import ISiteRoot
from Products.Five.browser import BrowserView
from Products.statusmessages.interfaces import IStatusMessage
from plone import api
from plone.dexterity.browser.view import DefaultView
from Products.Five.browser import BrowserView
from operator import itemgetter
import logging


logger = logging.getLogger("Joseba")




class UseCasesView(BrowserView):

    criteria='all'
    criteria_speaker='any'
    criteria_method='any'
    criteria_domain='any'
   
    def update(self):

         if "speaker-filter" in self.request.form:
            messages = IStatusMessage(self.request)
            try:
                self.criteria_domain = self.request.form['domain']
                self.criteria_method = self.request.form['method']
                #logger.info('criteria:::::::' + self.criteria)
                messages.add('criteria:::::::' + self.criteria_method + '::::::' + self.criteria_domain)
            except Exception as e:
                logger.exception(e)
                messages.add(u"It did not work out. This exception came when processing the form:" + unicode(e))
    def use_cases(self):
        results = []
        params={}
        if not self.criteria_domain is None and  not self.criteria_domain == 'any':
            params['valu3s_use_case_domain']= self.criteria_domain
        
        if len(params) == 0:
            brains = api.content.find(portal_type='use_case')      
        else:
            brains = api.content.find( portal_type='use_case', **params)          
        for brain in brains:
            use_case = brain.getObject()
            logger.info('USE CASE::::::'+str(use_case.use_case_number))            
            if self.hasQueryMethod(use_case):
                results.append({
                    'title': brain.Title,
                    'description': brain.Description,
                    'url': brain.getURL(),
                    'use_case_number': use_case.use_case_number,
                    #'domain': ', '.join(use_case.domain or []),
                    'evaluation_scenarios': ', '.join(self.scenarios(use_case) or []),
                    'use_case_methods': ', '.join(self.use_case_methods(use_case) or []),
                    'domain': use_case.use_case_domain,
                    'provider_ref': self.provider(use_case)
                    })
        return results
    def hasQueryMethod(self, use_case):
        result = False
        methods = []
        if not self.criteria_method == 'any':
            methods =  self.use_case_methods(use_case)
            if len(methods) > 0:
              for method in methods:
                    if self.criteria_method == method:
                        # skip broken relations
                        result = True
                        break
        else:
            result =True 
        return result
    def hasSpeaker(self, talk):
        result = False
        if not self.criteria_speaker == 'any':
            if not talk.speaker_list is None:
                for rel in talk.speaker_list:
                    if rel.isBroken():
                        # skip broken relations
                        continue
                    speaker = rel.to_object
                    if (speaker.name == self.criteria_speaker):
                        result = True
                        break
                    logger.info('Speaker Ref:::EMAIL:::' + str(speaker.email))
                    logger.info('Speaker Ref:::NAME:::' + str(speaker.name))
        else:
            result = True
        return result
            
    def speaker_refs(self, talk):
        results = []
        if not talk.speaker_list is None:
            for rel in talk.speaker_list:
                if rel.isBroken():
                    # skip broken relations
                    continue
                speaker = rel.to_object
                results.append(speaker.name)
        return results

    def log_talk(self, talk):
        logger.info('Brain Room::::::' + str(talk.room))
        logger.info('Brain Type_of_talk::::::' + str(talk.type_of_talk))
        logger.info('Brain Titlee::::::' + str(talk.Title))
        logger.info('Speaker Ref::::::' + str(talk.speaker_list))
        results = []
        if not talk.speaker_list is None:
            for rel in talk.speaker_list:
                if rel.isBroken():
                    continue
                speaker = rel.to_object
                logger.info('Speaker Ref:::EMAIL:::' + str(speaker.email))
                logger.info('Speaker Ref:::NAME:::' + str(speaker.name))

  
    '''Use case Scenario '''
    def   use_case_methods(self, use_case):
        results = []
        froga  = ['Aerospace','Agriculture', 'Automotive','Healthcare','Railway', 'Multi-domain']
        if not use_case.evaluation_scenario is None:
            for rel in use_case.evaluation_scenario:
                if rel.isBroken():
                    # skip broken relations
                    continue
                scenario = rel.to_object
                if not scenario is None:
                    methods = self.scenario_methods(scenario)
                    #l.extend([i for i in n if i not in l]
                    for method in methods:
                       if method not in results:
                            results.append(method)
                    #results.extend(methods)
        return results
    
    '''Use case Scanerio '''
    def   scenario_methods(self, scenario):
        results = []
        if not scenario.evaluation_scenario is None:
            if not scenario.evaluation_secnario_v_v_methods_list is None:
                for rel in scenario.evaluation_secnario_v_v_methods_list:
                    if rel.isBroken():
                        # skip broken relations
                        continue
                    method = rel.to_object
                    if not method is None:
                        results.append(method.title)
        return results

    '''Use case Scanerio '''
    def  scenarios(self, use_case):
        results = []
        if not use_case.evaluation_scenario is None:
            for rel in use_case.evaluation_scenario:
                if rel.isBroken():
                    # skip broken relations
                    continue
                scenario = rel.to_object
                if not scenario is None:
                    results.append(scenario.title)
        return results

    '''Providers from ref list'''
    def provider(self, use_case):
        result = 'Not defined'
        if not use_case.use_case_provider is None :
            rel = use_case.use_case_provider
            if not rel.isBroken() and not rel is None:
                provider = rel.to_object
                if not provider is None:
                    logger.info('PROVIDER:'+provider.title)
                    result = provider.title
        return result

    '''Speaker from ref list'''
    def domains(self):
        results = ['Aerospace','Agriculture', 'Automotive','Industrial Robotics', 'Healthcare','Railway', 'Multi-domain']
        return results
    
    def methods(self):
        results = []
        brains = api.content.find(portal_type='method')
        for brain in brains:
            method = brain.getObject()
            logger.info('METHOD:'+str(method.title))
            results.append({
                'title': brain.Title,
                'description': brain.Description,
                'url': brain.getURL()
                })
        return results
