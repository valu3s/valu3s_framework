from Products.CMFCore.interfaces import ISiteRoot
from Products.Five.browser import BrowserView
from Products.statusmessages.interfaces import IStatusMessage
from plone import api
from plone.dexterity.browser.view import DefaultView
from Products.Five.browser import BrowserView
from operator import itemgetter
import logging


logger = logging.getLogger("Joseba")




class YourViewName(BrowserView):

    criteria='all'
    criteria_speaker='any'
    def izenak(self):
        results = []
        results.append({
            'title': 'joseba',
        })
        #self.froga = self.request.form['filename']
        #logger.info('Call froga:::::::'+self.froga)
        return results
        #return self.froga

    def update(self):

         if "speaker-filter" in self.request.form:
            messages = IStatusMessage(self.request)
            try:
                # do something
                #messages.add("Button pressed")
                filename = self.request.form['filename']
                self.criteria = filename
                filename = self.request.form['speaker']
                self.criteria_speaker = filename
                logger.info('criteria:::::::' + self.criteria)
                

            except Exception as e:
                logger.exception(e)
                messages.add(u"It did not work out. This exception came when processing the form:" + unicode(e))
       

    def talks(self):
        results = []
        logger.info('BERRIA:fff::::::' + self.criteria)
        if self.criteria == 'all':
            brains = api.content.find(context=self.context, portal_type='talk')
            
        else:
            brains = api.content.find(context=self.context, portal_type='talk', type_of_talk=self.criteria)           
    
        for brain in brains:
            talk = brain.getObject()
            self.log_talk(talk)
            if  self.hasSpeaker(talk):
                results.append({
                    'title': brain.Title,
                    'description': brain.Description,
                    'url': brain.getURL(),
                    'type_of_talk': talk.type_of_talk,
                    'audience': ', '.join(talk.audience or []),
                    'speaker': talk.speaker,
                    'room': talk.room,
                    'uuid': talk.UID,
                    'speaker_ref': ', '.join(self.speaker_refs(talk) or [])
                    })
        return results
    def hasSpeaker(self, talk):
        result = False
        if not self.criteria_speaker == 'any':
            if not talk.speaker_list is None:
                for rel in talk.speaker_list:
                    if rel.isBroken():
                        # skip broken relations
                        continue
                    speaker = rel.to_object
                    if (speaker.name == self.criteria_speaker):
                        result = True
                        break
                    logger.info('Speaker Ref:::EMAIL:::' + str(speaker.email))
                    logger.info('Speaker Ref:::NAME:::' + str(speaker.name))
        else:
            result = True
        return result
            
    def speaker_refs(self, talk):
        results = []
        if not talk.speaker_list is None:
            for rel in talk.speaker_list:
                if rel.isBroken():
                    # skip broken relations
                    continue
                speaker = rel.to_object
                results.append(speaker.name)
        return results

    def log_talk(self, talk):
        logger.info('Brain Room::::::' + str(talk.room))
        logger.info('Brain Type_of_talk::::::' + str(talk.type_of_talk))
        logger.info('Brain Titlee::::::' + str(talk.Title))
        logger.info('Speaker Ref::::::' + str(talk.speaker_list))
        #logger.info('Speaker Ref:::len:::' + str(len(talk.speaker_list))) #NoneType
        results = []
        if not talk.speaker_list is None:
            for rel in talk.speaker_list:
                if rel.isBroken():
                    # skip broken relations
                    continue
                speaker = rel.to_object
                logger.info('Speaker Ref:::EMAIL:::' + str(speaker.email))
                logger.info('Speaker Ref:::NAME:::' + str(speaker.name))
            # obj = rel.to_object
            #if api.user.has_permission('View', obj=obj):
            #   results.append(obj)
        #if ( len(talk.speaker_list) > 0 ):
            #speaker = talk.speaker_list[0]
            #logger.info('Speaker Ref:::NAME:::' + str(speaker.Name))
            #logger.info('Speaker Ref:::EMAIL:::' + str(speaker.email))

        '''Speaker from ref list'''
    def speakers(self):
        results = []
        brains = api.content.find(portal_type='speaker')
        for brain in brains:
            speaker = brain.getObject()
            logger.info('SPEEAKER:'+str(speaker.name))
            results.append({
                'title': brain.Title,
                'description': brain.Description,
                'url': brain.getURL(),
                'name': speaker.name,
                'email': speaker.email
                })
        return results
