# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from valu3s.framework.testing import VALU3S_FRAMEWORK_INTEGRATION_TESTING  # noqa: E501

import unittest


try:
    from Products.CMFPlone.utils import get_installer
except ImportError:
    get_installer = None


class TestSetup(unittest.TestCase):
    """Test that valu3s.framework is properly installed."""

    layer = VALU3S_FRAMEWORK_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        if get_installer:
            self.installer = get_installer(self.portal, self.layer['request'])
        else:
            self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if valu3s.framework is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'valu3s.framework'))

    def test_browserlayer(self):
        """Test that IValu3SFrameworkLayer is registered."""
        from valu3s.framework.interfaces import (
            IValu3SFrameworkLayer)
        from plone.browserlayer import utils
        self.assertIn(
            IValu3SFrameworkLayer,
            utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = VALU3S_FRAMEWORK_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        if get_installer:
            self.installer = get_installer(self.portal, self.layer['request'])
        else:
            self.installer = api.portal.get_tool('portal_quickinstaller')
        roles_before = api.user.get_roles(TEST_USER_ID)
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.installer.uninstallProducts(['valu3s.framework'])
        setRoles(self.portal, TEST_USER_ID, roles_before)

    def test_product_uninstalled(self):
        """Test if valu3s.framework is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'valu3s.framework'))

    def test_browserlayer_removed(self):
        """Test that IValu3SFrameworkLayer is removed."""
        from valu3s.framework.interfaces import \
            IValu3SFrameworkLayer
        from plone.browserlayer import utils
        self.assertNotIn(
            IValu3SFrameworkLayer,
            utils.registered_layers())
