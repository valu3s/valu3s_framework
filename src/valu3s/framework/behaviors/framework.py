# -*- coding: utf-8 -*-

from plone.autoform.interfaces import IFormFieldProvider

from plone.supermodel import directives

from plone.supermodel import model
from z3c.relationfield.schema import RelationChoice
from z3c.relationfield.schema import RelationList
from zope.schema.vocabulary import SimpleVocabulary

from plone.autoform import directives as form
from z3c.form.browser.checkbox import CheckBoxFieldWidget
from z3c.form.browser.radio import RadioFieldWidget
from z3c.form.browser.orderedselect import OrderedSelectWidget
#from plone.formwidget.autocomplete import AutocompleteMultiFieldWidget
from plone.z3cform.textlines import TextLinesFieldWidget	
from z3c.form.browser import select
	


from zope import schema

from zope.interface import provider


@provider(IFormFieldProvider)
class IFramework(model.Schema):


    directives.fieldset(

        'VALU3S framework',

        label=u'VALU3S Framework',

        fields=['valu3s_evaluation_environment', 'valu3s_evaluation_type', 'valu3s_type_of_component_under_evaluation',
                'valu3s_evaluation_stage', 'valu3s_purpose_of_the_component_under_evaluation', 
                'valu3s_type_of_requirement_under_evaluation','valu3s_evaluation_performance_indicator'],

    )

   
    form.widget(valu3s_evaluation_environment=CheckBoxFieldWidget)
    valu3s_evaluation_environment = schema.List(
        title=u'Evaluation Environment Type',
        description=u"Type of environment where the method is applied. It can be more than one type of environment.(Dimension 1)",
        required=False,
        value_type= schema.Choice( title = (u"Dimnension1"),
                    vocabulary = SimpleVocabulary.fromValues([u'In-the-lab environment',u'Closed evaluation environment',u'Open evaluation environment']) ,
                    required=False, default=None),
        
    )

    form.widget(valu3s_evaluation_type=CheckBoxFieldWidget)
    valu3s_evaluation_type = schema.List(
        title=u'Evaluation Type',
        description=u"Type of evaluation performed with that method.  If a method is hybrid, more than one type can be selected.(Dimension 2)",
        required=False,
        value_type= schema.Choice( title = (u"Dimnension2"),
                    vocabulary = SimpleVocabulary.fromValues([u'Experimental - Testing',u'Experimental - Monitoring',
                                                            u'Experimental - Simulation',u'Analytical - Formal',
                                                            u'Analytical - Semi-Formal']) ,
                    required=False, default=None),
        
    )
    
    form.widget(valu3s_type_of_component_under_evaluation=CheckBoxFieldWidget)
    valu3s_type_of_component_under_evaluation = schema.List(
        title=u'Type of Component Under Evaluation',
        description=u"Type of components that can be evaluated with that method. It can be more than one type.(Dimension 3)",
        required=False,
        value_type= schema.Choice( title = (u"Dimnension3"),
                    vocabulary = SimpleVocabulary.fromValues([u'Model' , u'Software', u'Hardware']) ,
                    required=False, default=None),
        
    )

    valu3s_evaluation_stage  = schema.List(
        title=u'Evaluation Stage',
        description=u"The evaluation stage where the method is used.(Dimension 5)",
        required=False,
        value_type= schema.Choice( title = (u"Dimnension5"),
                    vocabulary = SimpleVocabulary.fromValues([u'Concept',u'Requirement Analysis',u'System Design',
                                                            u'Architecture Design', u'Detail  Design', u'Implementation',
                                                            u'Unit testing', u'Integration testing',u'System testing',u'Acceptance testing',
                                                            u'Operation', u'Risk analysis',u'Other']) ,
                    required=False, default=None),
        
    )


    valu3s_purpose_of_the_component_under_evaluation  = schema.List(
        title=u'Purpose  of the Component Under Evaluation',
        description=u"The type of purpose that can be evaluated by a method. It can be more than one type.(Dimension 6)",
        required=False,
        value_type= schema.Choice( title = (u"Dimnension6"),
                    vocabulary = SimpleVocabulary.fromValues([u'Sensing', u'Thinking', u'Acting', u'Other']) ,
                    required=False, default=None),
        
    )

    valu3s_type_of_requirement_under_evaluation = schema.List(
        title=u'Type of Requirement Under Evaluation',
        description=u"The type of requirements that a method is able to evaluate. It can be more than one type.(Dimension 7)",
        required=False,
        value_type= schema.Choice( title = (u"Dimnension7"),
                    vocabulary = SimpleVocabulary.fromValues([u'Non-Functional - Safety', u'Non-Functional - Security',
                                        u'Non-Functional - Privacy', u'Non-Functional - Other',u'Functional']) ,
                    required=False, default=None),
        
    )

    valu3s_evaluation_performance_indicator = schema.List(
        title=u'Evaluation Performance Indicator',
        description=u"The type of evaluation criteria or KPI that a method is able to improve. It can be more than one type.(Dimension 8)",
        required=False,
        value_type= schema.Choice( title = (u"Dimnension8"),
                    vocabulary = SimpleVocabulary.fromValues([u'V&V process criteria',u'SCP criteria']) ,
                    required=False, default=None),
        
    )


    